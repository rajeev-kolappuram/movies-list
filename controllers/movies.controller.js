async function moviesList(req, res, dbo) {
    var students = await dbo.collection("student").find({}).toArray();

    return res.status(200).json({ status: 1, message: 'File list.', data: students });
}

module.exports = {
    moviesList
}