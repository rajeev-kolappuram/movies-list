const express = require('express');
const MongoClient = require('mongodb').MongoClient;

const { moviesList } = require('../controllers/movies.controller');
const checkAuth = require('../helpers/checkAuth');

const router = express.Router();
const url = "mongodb://localhost:27017/";

var dbo;
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    dbo = db.db("gfg");
});

router.get('/file-list', checkAuth, (req, res) => {
    moviesList(req, res, dbo);
});

module.exports = router;