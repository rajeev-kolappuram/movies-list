const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');

const moviesRoute = require('./routes/movies');

dotenv.config();
const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));
app.use('/uploads', express.static('uploads'));

app.listen(port);

console.log('\n');
console.log("\x1b[36m%s\x1b[0m", "App Running Port " + port, '\n');

app.use('/movies', moviesRoute);

module.exports = { app };
