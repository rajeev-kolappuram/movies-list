module.exports = function (req, res, next) {
    if (req.headers && req.headers.authorization) {
        var authorization = req.headers.authorization.split(' ')[1], decoded;
        if(authorization === 'FSMovies2021') {
            next();
        } else {
            return res.status(422).json({ status: 0, message: 'Token not match.' });
        }
    } else {
        return res.status(422).json({ status: 0, message: 'Token not found.' });
    }
};